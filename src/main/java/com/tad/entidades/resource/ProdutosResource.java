package com.tad.entidades.resource;

import com.tad.entidades.model.Produto;
import com.tad.entidades.repository.Produtos;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/produtos")
public class ProdutosResource {
    @Autowired
    private Produtos produtos;

    @PostMapping
    public Produto adicionar(@Valid @RequestBody Produto produto) {
        return produtos.save(produto);
    }

    @GetMapping
    public List<Produto> listar() {
        return produtos.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Produto> buscar(@PathVariable Long id) {
        Produto contato = produtos.findOne(id);

        if (contato == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(contato);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Produto> atualizar(@PathVariable Long id,
                                             @Valid @RequestBody Produto produto) {
        Produto existente = produtos.findOne(id);

        if (existente == null) {
            return ResponseEntity.notFound().build();
        }

        BeanUtils.copyProperties(produto, existente, "id");

        existente = produtos.save(existente);

        return ResponseEntity.ok(existente);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> remover(@PathVariable Long id) {
        Produto contato = produtos.findOne(id);

        if (contato == null) {
            return ResponseEntity.notFound().build();
        }

        produtos.delete(contato);

        return ResponseEntity.noContent().build();
    }
}
