package com.tad.entidades.resource;

import com.tad.entidades.model.Filial;
import com.tad.entidades.repository.Filiais;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/filiais")
public class FiliaisResource {
    @Autowired
    private Filiais filiais;

    @PostMapping
    public Filial adicionar(@Valid @RequestBody Filial filial) {
        return filiais.save(filial);
    }

    @GetMapping
    public List<Filial> listar() {
        return filiais.findAll();
    }

    @GetMapping("/{id}")
    public ResponseEntity<Filial> buscar(@PathVariable Long id) {
        Filial filial = filiais.findOne(id);

        if (filial == null) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(filial);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Filial> atualizar(@PathVariable Long id,
                                             @Valid @RequestBody Filial filial) {
        Filial existente = filiais.findOne(id);

        if (existente == null) {
            return ResponseEntity.notFound().build();
        }

        BeanUtils.copyProperties(filial, existente, "id");

        existente = filiais.save(existente);

        return ResponseEntity.ok(existente);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> remover(@PathVariable Long id) {
        Filial filial = filiais.findOne(id);

        if (filial == null) {
            return ResponseEntity.notFound().build();
        }

        filiais.delete(filial);

        return ResponseEntity.noContent().build();
    }
}
