package com.tad.entidades.repository;

import com.tad.entidades.model.Filial;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Filiais extends JpaRepository<Filial, Long> {
}
