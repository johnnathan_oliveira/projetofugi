package com.tad.entidades.repository;

import com.tad.entidades.model.Produto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Produtos extends JpaRepository<Produto, Long> {

}
