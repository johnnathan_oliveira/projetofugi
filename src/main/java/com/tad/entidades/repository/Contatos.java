package com.tad.entidades.repository;

import com.tad.entidades.model.Contato;
import org.springframework.data.jpa.repository.JpaRepository;

public interface Contatos extends JpaRepository<Contato, Long> {

}
