package com.tad.entidades;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FujiokaTad {

	public static void main(String[] args) {
		SpringApplication.run(FujiokaTad.class, args);
	}
}
